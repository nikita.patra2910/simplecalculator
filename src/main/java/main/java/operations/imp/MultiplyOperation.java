package main.java.operations.imp;

import java.util.function.BinaryOperator;

public class MultiplyOperation implements BinaryOperator<Double> {

    @Override
    public Double apply(Double aDouble, Double aDouble2) {
        return aDouble * aDouble2;
    }
}
