package main.java;

import main.java.calculators.Calculator;
import main.java.calculators.impl.DynamicCalculatorImp;

import java.util.Scanner;
import java.util.function.BinaryOperator;
import java.util.function.Supplier;
import java.util.regex.Pattern;

public class AnonymousRunner {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Calculator calculator = DynamicCalculatorImp.builder()
                .addOperation("+", new BinaryOperator<Double>() {
                    @Override
                    public Double apply(Double firstNumber, Double secondNumber) {
                        return firstNumber + secondNumber;
                    }
                })
                .addOperation("-", new BinaryOperator<Double>() {
                    @Override
                    public Double apply(Double firstNumber, Double secondNumber) {
                        return firstNumber + secondNumber;
                    }
                })
                .addOperation("-", new BinaryOperator<Double>() {
                    @Override
                    public Double apply(Double firstNumber, Double secondNumber) {
                        return firstNumber + secondNumber;
                    }
                })
                .addOperation("-", new BinaryOperator<Double>() {
                    @Override
                    public Double apply(Double firstNumber, Double secondNumber) {
                        return firstNumber + secondNumber;
                    }
                })
                .addOperation("-", new BinaryOperator<Double>() {
                    @Override
                    public Double apply(Double firstNumber, Double secondNumber) {
                        return firstNumber + secondNumber;
                    }
                })
                .build();

        String input;
        while (!(input = scanner.nextLine()).equals("--exit")) {
            if (input.equals("--help")) {
                System.out.println(calculator.getAvailableOperations());
                continue;
            }
            System.out.print("-->");
            //todo add simply input validation
            //todo adjust this impementation
            Pattern pattern = Pattern.compile("^\\d*\\.?\\d*\\s[-/*^+]\\s\\d*\\.?\\d*\\$");
            if (pattern.matcher(input).find()) {
                System.out.println("Wrong input. Please try again.");
                continue;
            }
            String[] inputArguments = input.split(" ");
            Supplier<Double> firstNumber = () -> Double.valueOf(inputArguments[0]);
            String operator = inputArguments[1];
            Supplier <Double> secondNumber = () -> Double.valueOf(inputArguments[2]);
            System.out.println("-->" +  calculator.doOperation(operator, firstNumber.get(), secondNumber.get()));
        }
    }
}
