package main.java.calculators;

import java.util.Set;

public interface Calculator {
    Double doOperation (String operator, Double firstNumber, Double secondNumber);
    Set<String> getAvailableOperations();
}
