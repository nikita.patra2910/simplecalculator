package user.service;

import user.dto.User;
import user.generator.UserGenerator;
import user.repository.UserRepository;
import user.sort.Sort;
import user.sort.map.SortedParams;
import utils.DateProvider;

import java.util.*;
import java.util.function.Predicate;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static user.sort.SortType.ASC;

public class UserServiceImpl implements UserService {
    private final UserGenerator userGenerator;
    private final UserRepository userRepository;
    private final DateProvider dateProvider;

    public UserServiceImpl(
            UserGenerator userGenerator,
            UserRepository userRepository,
            DateProvider dateProvider) {
        this.userGenerator = userGenerator;
        this.userRepository = userRepository;
        this.dateProvider = dateProvider;
    }

    @Override
    public List<User> generate(int number) {
        return userGenerator.generateUsers(number);
    }

    @Override
    public long save(List<User> users) {
        long count = 0L;
        for (User user : users) {
            User add = userRepository.add(user);
            count++;
        }
        return count;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User oldestUser() {
        return userRepository.getAll().stream()
                .max(Comparator.comparing(user -> dateProvider.calculateWholeYearsCountFromPeriod(user.getBirthday())))
                .orElse(null);
    }

    @Override
    public Map<Integer, List<User>> getAgeUserGroup() {
        return userRepository.getAll().stream()
                .collect(groupingBy(user -> dateProvider.calculateWholeYearsCountFromPeriod(user.getBirthday())));
    }

    @Override
    public List<User> search(Predicate<User> userFilter) {
        return userRepository.getAll().stream()
                .filter(userFilter)
                .collect(toList());
    }

    @Override
    public List<User> sort(Sort sort) {
        return userRepository.getAll().stream()
                .sorted(sort.getComparator())
                .collect(toList());
    }

    @Override
    public List<User> multipleSort(List<Sort> sort) {
        Comparator<User> userComparator = sort.stream()
                .map(Sort::getComparator)
                .reduce(Comparator::thenComparing)
                .orElse(Comparator.comparing(User::getId));

        return userRepository.getAll().stream()
                .sorted(userComparator)
                .collect(toList());
    }

    public List<User> multiSortRefactoring(List<SortedParams> sortedParams) {

        List<Comparator<User>> comparators = new ArrayList<>();

        for (SortedParams sortedParam : sortedParams) {
            if (sortedParam.getSortType() == ASC) {
                comparators.add(Comparator.comparing(sortedParam.getSortField()));
            } else {
                comparators.add(Comparator.comparing(sortedParam.getSortField()).reversed());
            }
        }
        return userRepository.getAll().stream()
                .sorted(comparators.stream()
                        .reduce(Comparator::thenComparing)
                        .orElse(Comparator.comparing(User::getId)))
                .collect(toList());
    }
}

