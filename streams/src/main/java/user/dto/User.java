package user.dto;

import java.time.LocalDate;
import java.util.Objects;
import java.util.function.Predicate;

public class User {
   private Integer id;
   private String firstName;
   private String lastName;
   private Boolean isMarried;
   private LocalDate birthday;

   public boolean isMeried(User user) {
      return user.isMarried;
   }

   public Integer getId() {
      return id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   public String getFirstName() {
      return firstName;
   }

   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }

   public String getLastName() {
      return lastName;
   }

   public void setLastName(String lastName) {
      this.lastName = lastName;
   }

   public boolean isMarried() {
      return isMarried;
   }

   public void setMarried(boolean married) {
      isMarried = married;
   }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

   @Override
   public String toString() {
      return "User{" +
          "id=" + id +
          ", firstName='" + firstName + '\'' +
          ", lastName='" + lastName + '\'' +
          ", isMarried=" + isMarried +
          ", birthday=" + birthday +
          '}';
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      User user = (User) o;
      return  isMarried == user.isMarried &&
              Objects.equals(firstName, user.firstName) &&
              Objects.equals(lastName, user.lastName) &&
              Objects.equals(birthday, user.birthday);
   }

   @Override
   public int hashCode() {
      return Objects.hash(firstName, lastName, isMarried, birthday);
   }
}
