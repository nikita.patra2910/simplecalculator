package user.generator;

import user.dto.User;
import utils.DateProvider;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class UserGeneratorImpl implements UserGenerator{
    private List<String> firstNames = List.of("Liam", "Noah",
            "William", "James", "Logan", "Mason", "Elijan", "Oliver",
            "Aiden", "Henry", "Joseph", "Samuel", "Luke", "Jack", "Levi",
            "Kate", "Abby", "Amber", "Anita", "Caren", "Carol", "Hannah");

    private List<String> lastNames = List.of("Smith", "Hall", "Johnson", "Williams",
            "Jones", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor",
            "Thomas", "Jakson", "White", "Baker", "Cox", "Perry", "Long");

    private Random random = new Random();
    DateProvider dateProvider = new DateProvider();

    @Override
    public User generateUser() {
        String firstName = firstNames.get(random.nextInt(firstNames.size()-1));
        String lastName = lastNames.get(random.nextInt(lastNames.size()-1));
        LocalDate birthday = dateProvider.generateRandomDate();
        int age = dateProvider.calculateWholeYearsCountFromPeriod(birthday);
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMarried(random.nextBoolean());
        user.setBirthday(birthday);
        return user;
    }

    @Override
    public List<User> generateUsers(int count) {
        return IntStream.range(0, count)
                .mapToObj(i-> generateUser())
                .collect(toList());
    }
}
