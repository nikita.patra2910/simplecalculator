package user.sort.map;

import user.dto.User;
import user.sort.SortType;

import java.util.function.Function;

public class SortedParams {
    private final SortType sortType;
    private final Function <User, Comparable> sortField;


    public SortedParams(SortType sortType, Function<User, Comparable> sortField) {
        this.sortType = sortType;
        this.sortField = sortField;
    }

    public SortType getSortType() {
        return sortType;
    }

    public Function<User, Comparable> getSortField() {
        return sortField;
    }
}
