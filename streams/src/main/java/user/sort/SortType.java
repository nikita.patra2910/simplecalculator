package user.sort;

public enum SortType {
    ASC,
    DESC
}
