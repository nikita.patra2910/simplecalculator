package user.repository;

import user.dto.User;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class UserRepositoryInMemory implements UserRepository {
    private Map<Integer, User> usersStorage = new HashMap<>();
    private AtomicInteger id = new AtomicInteger(0);

    @Override
    public User add(User user) {
        int userId = id.getAndIncrement();
        user.setId(userId);
        usersStorage.put(userId, user);
        return user;
    }

    @Override
    public long add(List<User> users) {
        for (User user : users) {
            int userId = id.incrementAndGet();
            user.setId(userId);
            usersStorage.put(userId, user);
        }
        return users.size();
    }

    @Override
    public User get(int id) {
        return usersStorage.getOrDefault(id, null);
    }

    @Override
    public List<User> getAll() {
        return new ArrayList<>(usersStorage.values());
    }

    @Override
    public User update(User user) {
        if (usersStorage.containsKey(user.getId())) {
            usersStorage.replace(user.getId(), user);
            return usersStorage.get(user.getId());
        }
        else
            return null;
    }

    @Override
    public boolean delete(User user) {
        if (usersStorage.containsKey(user.getId())) {
            usersStorage.remove(user.getId());
            return true;
        }
        else
            return false;
    }

    @Override
    public void deleteAll() {
        usersStorage.clear();
    }
}





