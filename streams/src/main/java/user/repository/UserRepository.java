package user.repository;

import user.dto.User;

import java.util.List;

public interface UserRepository {

    User add(User user);

    long add(List<User> users);

    User get(int id);

    List<User> getAll();

    User update(User user);

    boolean delete(User user);

    void deleteAll();
}