package number.generator;

import number.generator.exceptions.AssertionException;
import number.generator.exceptions.StorageException;
import user.db.connection.DataBaseConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/*todo:
   1. Run it
   2. Optimize it
*/
public class NumberStorageImpl implements NumberStorage {
    private final Connection connection;

    public NumberStorageImpl(DataBaseConnectionProvider provider) {
        connection = provider.getDbConnection();
    }

    @Override
    public void insert(Map<Integer, Object> params) {
        String queryPrefix = "INSERT INTO numbers (number) VALUES ";
        if (isNull(params) || params.isEmpty()) {
            return;
        }
        else {
            StringBuilder querySuffix = new StringBuilder("(?)");
            for (int i = 1; i<params.size(); i++) {
                querySuffix.append(", (?)");
            }
            String query = queryPrefix + querySuffix + ";";
            executeUpdate(query, params);
        }

    }

    @Override
    public int insert(Integer number) {
        String query = "INSERT INTO numbers (number) VALUES (?) ";
        executeUpdate(query, Map.of(1, number));
        return number;
    }

    @Override
    public Integer get(int id) {
        String query = "SELECT number FROM numbers WHERE number_id = ?";
        int number = 0;
        try (ResultSet rs = executeQuery((query), List.of(id))) {
            while (rs.next()) {
                number = rs.getInt("number");
            }
            return number;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new StorageException("Couldn't get number with id " + id + " from table", e);
        }
    }

    @Override
    public List<Integer> getAll() {
        String query = "SELECT number FROM numbers";
        List<Integer> nums = new ArrayList<>();
        try (ResultSet rs = executeQuery((query))) {
            while (rs.next()) {
                int number = rs.getInt("number");
                nums.add(number);
            }
            return nums;
        } catch (SQLException e) {
            throw new StorageException("Couldn't get all data from table", e);
        }
    }

    @Override
    public void update (Integer number, Integer id){
        executeUpdate(
                    "UPDATE numbers SET number = ? where number_id = ?",
                    Map.of(1, number, 2, id)
            );
    }

    @Override
    public void delete (Integer id){
        executeUpdate(
                    "DELETE FROM numbers WHERE number_id = ?",
                    Map.of(1, id));
    }

    @Override
    public void deleteAll () {
        executeUpdate("Truncate table numbers RESTART IDENTITY", emptyMap());
    }

    @Override
    public void close () throws AssertionException {
        if (nonNull(this.connection)) {
           try {
                connection.close();
            } catch (Exception e) {
                throw new StorageException("Connection couldn't close", e);
            }
        }
    }

    private ResultSet executeQuery (String query){
            return this.executeQuery(query, emptyList());
        }

    private ResultSet executeQuery(String query, List<Integer> params) {
        ResultSet rs;
        try {
            PreparedStatement ps = connection.prepareStatement(query);
            if (!params.isEmpty()) {
                for (int index = 1; index <= params.size(); ++index) {
                    ps.setInt(index, params.get(index-1));
                }
            }
            rs = ps.executeQuery();
        } catch (SQLException e) {
            throw new StorageException("Couldn't execute query " + query + ".Result Set wasn't returned", e);
        }
        return rs;
    }

    private void executeUpdate (String query, Map<Integer, Object> params){
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            Set<Map.Entry<Integer, Object>> entryMap = params.entrySet();
            entryMap.forEach(e -> {
                try {
                    ps.setObject(e.getKey(), e.getValue());
                } catch (SQLException ex) {
                    throw new StorageException("Couldn't set object for given params", ex);
                }
            });
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new StorageException("Couldn't update table by query " + query, e);
        }
    }
}
