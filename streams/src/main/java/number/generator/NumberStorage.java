package number.generator;

import java.util.List;
import java.util.Map;

public interface NumberStorage {
    void insert(Map<Integer, Object> params);

    int insert(Integer number);

    Integer get(int id);

    List<Integer> getAll();

    void update(Integer id, Integer number);

    void delete(Integer id);

    void deleteAll();

    void close();
}
