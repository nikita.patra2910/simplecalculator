package square.equation.solution.processing.impl;

import square.equation.solution.Solution;
import square.equation.solution.processing.rendering.InputParameters;

import static java.lang.String.valueOf;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class ReportResultBuilder {
    private final StringBuilder resultReport;

    private ReportResultBuilder(StringBuilder resultReport) {
        this.resultReport = resultReport;
    }

    public static ReportResultBuilder builder() {
        return new ReportResultBuilder(new StringBuilder());
    }

    public ReportResultBuilder addInputParameters(InputParameters ip) {
         this.addField("a = ", ip.getA());
         this.addField("b = ", ip.getB());
         this.addField("c = ", ip.getC());

        return this;
    }

    public ReportResultBuilder addSolution(Solution solution) {
        Double x1 = solution.getX1();
        Double x2 = solution.getX2();
        if (isNull(x1) && isNull(x2)) {
            this.addField("The solution is not natural number");
            return this;
        }

        this.addField("x1 = ", x1);

        if (nonNull(x2)) {
            this.addField("x2 = ", x2);
        }

        return this;
    }

    public String buildReport() {
        return resultReport.toString();
    }

    private void addField(String label, String value) {
        if (nonNull(value)) {
            resultReport.append("\t")
                    .append(label)
                    .append(" : ")
                    .append(value)
                    .append("\n");
        }
    }

    private void addField(String label, Number value) {
        if (nonNull(value)) {
            this.addField(label, valueOf(value));
        }
    }

    private void addField(String message) {
        resultReport.append(message).append("\n");
    }
}