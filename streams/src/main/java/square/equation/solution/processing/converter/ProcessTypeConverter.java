package square.equation.solution.processing.converter;

import square.equation.solution.processing.ProcessType;

public class ProcessTypeConverter {

    public static ProcessType toProcessType(String outputMode) {
        switch (outputMode) {
            case ("CONSOLE"):
                return ProcessType.CONSOLE;
            case ("FILE"):
                return ProcessType.FILE;
            default:
                throw new IllegalStateException("Unexpected output mode: " + outputMode);
        }
    }
}
