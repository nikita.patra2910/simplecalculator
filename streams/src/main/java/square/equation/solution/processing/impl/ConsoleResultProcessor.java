package square.equation.solution.processing.impl;

import square.equation.solution.Solution;
import square.equation.solution.processing.ResultProcessor;
import square.equation.solution.processing.rendering.InputParameters;
import square.equation.solution.processing.rendering.Render;

import java.util.List;

public class ConsoleResultProcessor implements ResultProcessor {
    private final List<Render> renders;

    public ConsoleResultProcessor(List<Render> renders) {
        this.renders = renders;
    }

    @Override
    public void processResult(Solution solution, InputParameters inputParameters) {
        renders.stream()
                .filter(render -> render.canRender(solution))
                .findFirst()
                .ifPresent(render -> render.renderResults(solution));
    }

    @Override
    public void shutDown() {
    }
}
