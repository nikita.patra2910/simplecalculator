package square.equation.solution.processing;

import square.equation.solution.Solution;
import square.equation.solution.processing.rendering.InputParameters;

import java.io.IOException;

public interface ResultProcessor {
    void processResult(Solution solution, InputParameters inputParameters) throws IOException;

    void shutDown();
}
