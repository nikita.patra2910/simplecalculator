package square.equation.solution.processing.rendering;

import square.equation.solution.Solution;

public interface Render {
    boolean canRender(Solution solution);

    void renderResults(Solution solution);
}
