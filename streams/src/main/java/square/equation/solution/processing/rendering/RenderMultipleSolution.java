package square.equation.solution.processing.rendering;

import square.equation.solution.ResultType;
import square.equation.solution.Solution;

public class RenderMultipleSolution implements Render {

      @Override
    public boolean canRender(Solution solution) {
        return solution.getResultType() == ResultType.MULTIPLE;
    }

    @Override
    public void renderResults(Solution solution) {
        System.out.printf("x1 = %f.2\n", solution.getX1());
        System.out.printf("x2 = %f.2", solution.getX2());
    }
}
