package square.equation.solution.impl;

import square.equation.solution.ResultType;
import square.equation.solution.Solution;

public class SingleSolution extends Solution {
    public SingleSolution(Double x1) {
        super(x1, null, ResultType.SINGLE);
    }
}
