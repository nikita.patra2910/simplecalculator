package square.equation.solution.impl;

import square.equation.solution.ResultType;
import square.equation.solution.Solution;

public class MultipleSolution extends Solution {
    public MultipleSolution(Double x1, Double x2) {
        super(x1, x2, ResultType.MULTIPLE);
    }
}
