package square.equation.solution.impl;

import square.equation.solution.ResultType;
import square.equation.solution.Solution;

public class NoneResultSolution extends Solution {
    public NoneResultSolution() {
        super(null, null, ResultType.NONE);
    }
}
