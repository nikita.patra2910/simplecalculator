package square.equation.solution;

public abstract class Solution {
    protected final Double x1;
    protected final Double x2;
    protected ResultType resultType;

    protected Solution(Double x1, Double x2, ResultType resultType) {
        this.x1 = x1;
        this.x2 = x2;
        this.resultType = resultType;
    }

    public ResultType getResultType() {
        return this.resultType;
    }

    public Double getX1() {
        return this.x1;
    }

    public Double getX2() {
        return this.x2;
    }
}
