import user.generator.NumberGenerator;
import user.generator.UserGenerator;
import user.generator.UserGeneratorImpl;
import user.repository.UserRepository;
import user.repository.UserRepositoryDB;
import user.repository.UserRepositoryInMemory;
import utils.DateProvider;

import java.sql.SQLException;
import java.util.List;

public class Application {

    public static void main(String... strings) throws SQLException, ClassNotFoundException {
        NumberGenerator numberGenerator = new NumberGenerator();
        UserGenerator userGenerator = new UserGeneratorImpl();
        UserRepository userRepository = new UserRepositoryInMemory();
        DateProvider dateProvider = new DateProvider();
        UserRepositoryDB dBRepository = new UserRepositoryDB();
//        QueryExecution queryExecution = new QueryExecution();

//        try {
//            DataBaseConnectionProvider.getDbConnection();
//        } catch (SQLException e) {
//            System.out.println("Connection is failed");
//            e.printStackTrace();
//        }
//        Statement stat = DataBaseConnection.createStatement();
//        stat.executeQuery(createQuery(numberGenerator));
//
//        UserServiceImpl userService = new UserServiceImpl(userGenerator, userRepository, dateProvider);
//        List<User> users = userService.generate(1000);
//        userService.save(users);
    }

        public static String createQuery (NumberGenerator numberGenerator){
            List<Integer> numbers = numberGenerator.generateNumbers(100);
            String query = "INSERT INTO Numbers (number) VALUES ";
            for (int i = 0; i <= numbers.size() - 2; i++) {
                query = query + "('" + numbers.get(i) + "'),";
            }
            return query + "('" + numbers.get(numbers.size() - 1) + "')";
        }

    }
