package collect;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public interface Collections {
    <E> List<E> filter(List<E> elements, Predicate<E> filter);

    <E> boolean anyMatch(List<E> elements, Predicate<E> predicate);

    <E> boolean allMatch(List<E> elements, Predicate<E> predicate);

    <E> boolean noneMatch(List<E> elements, Predicate<E> predicate);

    <T, R> List<R> map(List<T> elements, Function<T, R> mappingFunction);

    <E> void forEach(List<E> elements, Consumer<E> consumer);

    <E> Optional<E> max(List<E> elements, Comparator<E> comparator);

    <E> Optional<E> min(List<E> elements, Comparator<E> comparator);

    <E> List<E> distinct(List<E> elements);

    <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator);

    <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator);

    <E> Map<Boolean, List<E>> partitionBy(List<E> elements, Predicate<E> predicate);

    <T, K> Map<K, List<T>> groupBy(List<T> elements, Function<T, K> classifier);

    <T, K, U> Map<K, U> toMap(
            List<T> elements,
            Function<T, K> keyFunction,
            Function<T, U> valueFunction,
            BinaryOperator<U> mergeFunction);


}
