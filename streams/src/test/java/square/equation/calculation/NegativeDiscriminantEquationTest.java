package square.equation.calculation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import square.equation.solution.Solution;

import static org.junit.jupiter.api.Assertions.*;

class NegativeDiscriminantEquationTest {

    NegativeDiscriminantEquation negativeDiscriminantEquation;

    @BeforeEach
    public void setup() {
        negativeDiscriminantEquation = new NegativeDiscriminantEquation();
    }

    @Test
    void negativeDiscriminantEquation_should_calculate_result_for_negative_discriminant() {
        assertTrue(negativeDiscriminantEquation.isCalculated(-1));
        assertFalse(negativeDiscriminantEquation.isCalculated(0));
        assertFalse(negativeDiscriminantEquation.isCalculated(2));
    }

    @ParameterizedTest
    @CsvSource({"1, 2, -1.0", "-1, -2, -2.0", "2, 0, -2.0"})
    void test_calculate_square_equation_with_negative_discriminant(
            Integer a,
            Integer b,
            Double discriminant
    ) {
        Solution actual = negativeDiscriminantEquation.calculate(a, b, discriminant);
        assertNull(actual.getX1());
        assertNull(actual.getX2());
    }
}