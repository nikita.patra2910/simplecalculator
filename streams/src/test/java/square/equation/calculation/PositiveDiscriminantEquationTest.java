package square.equation.calculation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import square.equation.solution.Solution;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static square.equation.calculation.SquareEquation.calculateDiscriminant;

class PositiveDiscriminantEquationTest {
    private PositiveDiscriminantEquation positiveDiscriminantEquation;

    @BeforeEach
    void setUp() {
        positiveDiscriminantEquation = new PositiveDiscriminantEquation();
    }

    @Test
    void positiveDiscriminantEquation_should_calculate_result_for_positive_discriminant() {
        assertTrue(positiveDiscriminantEquation.isCalculated(1));
        assertFalse(positiveDiscriminantEquation.isCalculated(0));
        assertFalse(positiveDiscriminantEquation.isCalculated(-1));
    }

    @ParameterizedTest
    @MethodSource("provideTestData")
    void test_calculate_square_equation_with_positive_discriminant(
            Integer a,
            Integer b,
            Double discriminant,
            Double x1,
            Double x2
    ) {
        Solution actual = positiveDiscriminantEquation.calculate(a, b, discriminant);
        assertNotNull(actual.getX1());
        assertEquals(actual.getX1(), x1);
        assertNotNull(actual.getX2());
        assertEquals(actual.getX2(), x2);
    }

    private static Stream<Arguments> provideTestData() {
        return Stream.of(
                Arguments.of(1, 3, calculateDiscriminant(1, 3, 2), -1.0, -2.0),
                Arguments.of(-1, -3, 1.0, -2.0, -1.0),
                Arguments.of(2, 2, 4.0, 0., -1.0)
        );
    }
}