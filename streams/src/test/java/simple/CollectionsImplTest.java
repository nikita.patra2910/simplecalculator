package simple;

import collect.CollectionsImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import user.dto.User;
import utils.DateProvider;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static simple.UserFactory.createUser;

public class CollectionsImplTest {

    private CollectionsImpl collectionImpl;
    private DateProvider dateProvider;

    @BeforeEach
    public void setUp() {
        collectionImpl = new CollectionsImpl();
        dateProvider = new DateProvider();
    }

    @Test
    public void filter_should_return_integers_list() {
        List<Integer> expected = List.of(35, 100);
        List<Integer> actual = collectionImpl.filter(List.of(1, 20, 35, 100, -20), el -> el > 20);
        assertEquals(expected, actual);
    }

    @Test
    public void filter_should_return_string_list() {
        List<String> elements = List.of("John", "Dev", "Fabrice", "Ed");
        List<String> expected = List.of("John", "Fabrice");

        assertEquals(expected, collectionImpl.filter(elements, str -> str.length() >= 4));
    }

    @Test
    public void filter_should_return_users_list() {
        List<User> users = List.of(
                createUser("foo", "boo", false,
                        LocalDate.of(2009, 1, 12)),
                createUser("bar","boo", true,
                        LocalDate.of(1994, 11, 5)),
                createUser("baz","boo", true,
                        LocalDate.of(1989, 10, 25))
        );
        List<User> expected = List.of(createUser("baz","boo", true,
                LocalDate.of(1989, 10, 25)));
        assertEquals(expected, collectionImpl.filter(users,
                user -> dateProvider.calculateWholeYearsCountFromPeriod(user.getBirthday()) > 29));
    }

    @Test
    public void anymatch_int_to_int_list() {
        assertTrue(collectionImpl.anyMatch(List.of(1, 20, 35, 100, -20), el -> el == 100));
    }

    @Test
    public void anymatch_string_to_string_list() {
        assertTrue(collectionImpl.anyMatch(List.of("John", "Dev", "Fabrice", "Ed"), str -> str.equals("Ed")));
    }

    @Test
    public void anymatch_user_to_user_list() {
        List<User> users = List.of(
                createUser("foo","boo", false,
                        LocalDate.of(2009, 1, 12)),
                createUser("bar","boo", true,
                        LocalDate.of(1994, 11, 5)),
                createUser("baz","boo", true,
                        LocalDate.of(1989, 10, 25))
        );
        assertTrue(collectionImpl.anyMatch(users, us -> !us.isMarried()));
    }

    @Test
    public void allmatch_of_int_list() {
        assertFalse(collectionImpl.allMatch(List.of(1, 20, 35, 100, -20), el -> el > 0));
    }

    @Test
    public void allmatch_of_string_list() {
        assertTrue(collectionImpl.allMatch(List.of("Dev", "Fabrice", "Red"), str -> str.contains("e")));
    }

    @Test
    public void allmatch_users_by_user_name_condition() {
        List<User> users = List.of(
                createUser("foo","boo", false,
                        LocalDate.of(2009, 1, 12)),
                createUser("bar","boo", true,
                        LocalDate.of(1994, 11, 5)),
                createUser("baz","boo", true,
                        LocalDate.of(1989, 10, 25))
        );
        assertTrue(collectionImpl.allMatch(users, us -> us.getFirstName().length() == 3));
    }

    @Test
    public void nonematch_int_for_int_list() {
        assertTrue(collectionImpl.noneMatch(List.of(1, 20, 35, 100, -20), el -> el > 100));
    }

    @Test
    public void nonematch_string_for_string_list() {
        assertTrue(collectionImpl.noneMatch(List.of("John", "Dev", "Fabrice", "Ed"), str -> str.contains("l")));
    }

    @Test
    public void nonematch_users_by_age() {
        List<User> users = List.of(
                createUser("foo","boo", false,
                        LocalDate.of(2009, 1, 12)),
                createUser("bar","boo", true,
                        LocalDate.of(1994, 11, 5)),
                createUser("baz","boo", true,
                        LocalDate.of(1989, 10, 25))
        );
        assertTrue(collectionImpl.noneMatch(users,
                us -> dateProvider.calculateWholeYearsCountFromPeriod(us.getBirthday()) < 10));
    }

    @Test
    public void map_should_return_string_list_from_int_list() {
        List<String> expected = List.of("5 number", "7 number", "12 number", "15 number");
        List<String> actual = collectionImpl.map(List.of(5, 7, 12, 15), (e -> e.toString() + " number"));
        assertEquals(expected, actual);
    }

    @Test
    public void map_should_return_int_list_from_string_list() {
        List<Integer> expected = List.of(3, 4, 5);
        List<Integer> actual = collectionImpl.map(List.of("AAA", "BBBB", "CCCCC"), str -> str.length());
        assertEquals(expected, actual);
    }

    @Test
    public void map_should_return_user_names_from_user_list() {
        List<User> users = List.of(
                createUser("Bob","boo", false,
                        LocalDate.of(2009, 1, 12)),
                createUser("Bill","boo", true,
                        LocalDate.of(1994, 11, 5)),
                createUser("Ben","boo", true,
                        LocalDate.of(1989, 10, 25))
        );
        List<String> expected = List.of("Bob", "Bill", "Ben");
        List<String> actual = collectionImpl.map(users, User::getFirstName);
        assertEquals(expected, actual);
    }

    @Test
    public void foreach_should_copy_int_list_and_compare_with_original() {
        List<Integer> first = new ArrayList<>();
        first.add(12);
        first.add(16);
        first.add(18);
        first.add(25);
        List<Integer> second = new ArrayList<>();
        collectionImpl.forEach(first, second::add);
        assertEquals(first, second);
    }

    @Test
    public void foreach_should_copy_string_list_and_compare_with_original() {
        List<String> first = new ArrayList<>();
        first.add("aaa");
        first.add("bbb");
        first.add("ccc");
        List<String> second = new ArrayList<>();
        collectionImpl.forEach(first, second::add);
        assertEquals(first, second);
    }

    @Test
    public void foreach_should_copy_user_list_and_compare_with_original() {
        List<User> users = List.of(
                createUser("Bob","boo", false,
                        LocalDate.of(2009, 1, 12)),
                createUser("Bill","boo", true,
                        LocalDate.of(1994, 11, 5)),
                createUser("Ben","boo", true,
                        LocalDate.of(1989, 10, 25))
        );

        List<User> second = new ArrayList<>();

        collectionImpl.forEach(users, user -> second.add(user));

        assertEquals(users, second);
    }

    @Test
    public void max_should_return_max_from_int_list() {
        List<Integer> numbers = List.of(100, 200, 300, 400);
        Optional<Integer> actualOptional = collectionImpl.max(
                numbers,
                (num1, num2) -> (num1 > num2) ? 1 : (num1 < num2) ? -1 : 0
        );

        actualOptional.ifPresent(max -> assertEquals(400, (int) actualOptional.get()));

        assertTrue(actualOptional.isPresent());
        assertEquals(400, (int) actualOptional.get());
    }

    @Test
    public void max_should_return_max_from_string_list_by_first_char() {
        String expected = "fooze";
        Optional<String> actual = collectionImpl.max(List.of("foo", "fooz", "fooze"), String::compareTo);

        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    public void max_should_return_max_from_user_list_by_age() {
        List<User> users = List.of(
                createUser(LocalDate.of(2009, 1, 12)),
                createUser(LocalDate.of(1994, 11, 5)),
                createUser(LocalDate.of(1989, 10, 25))
        );

        User expected = createUser(LocalDate.of(1989, 10, 25));

        Comparator<User> userComparator = (user1, user2) -> {
            if (user1.getBirthday()
                    .isBefore(user2.getBirthday())) {
                return 1;
            }
            if (user1.getBirthday()
                    .isAfter(user2.getBirthday())) {
                return -1;
            }
            return 0;
        };

        Optional<User> actual = collectionImpl.max(users, userComparator);

        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    public void max_should_return_optional_empty_for_empty_collection() {
        Optional<Integer> actual = collectionImpl.max(List.of(), Integer::compareTo);

        assertTrue(actual.isEmpty());
    }

    @Test
    public void min_should_find_min_from_int_list() {
        List<Integer> numbers = List.of(12, 25, -10, 3);
        assertEquals(Optional.of(-10), collectionImpl.min(numbers, (num1, num2) -> {
            if (num2 < num1) {
                return 1;
            } else if (num1 < num2) {
                return -1;
            } else return 0;
        }));
    }

    @Test
    public void min_should_return_min_string_from_list() {
        List<String> strings = List.of("foo", "baz", "aaaa", "ooooo");

        Optional<String> actual = collectionImpl.min(
                strings,
                (str1, str2) -> {
                    String min = (str1.length() < str2.length()) ? str1 : str2;
                    for (int i = 0; i < min.length(); i++) {
                        if (str1.charAt(i) != str2.charAt(i)) {
                            return str1.charAt(i) - str2.charAt(i);
                        }
                    }
                    return str1.length() - str2.length();
                }
        );

        assertTrue(actual.isPresent());
        assertEquals("aaaa", actual.get());
    }

    @Test
    public void min_should_return_the_youngest_user_from_user_list() {
        List<User> users = List.of(
                createUser("Bob","boo", false,
                        LocalDate.of(2009, 1, 12)),
                createUser("Bill","boo", true,
                        LocalDate.of(1994, 11, 5)),
                createUser("Ben","boo", true,
                        LocalDate.of(1989, 10, 25))
        );

        User expected = createUser("Bob","boo", false,
                LocalDate.of(2009, 1, 12));

        Optional<User> actual = collectionImpl.min(
                users,
                (user1, user2) -> {
                    if (user1.getBirthday().isBefore(user2.getBirthday())) {
                        return 1;
                    }
                    if (user1.getBirthday().isAfter(user2.getBirthday())) {
                        return -1;
                    }
                    return 0;
                });

        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    public void distinct_should_return_int_list_without_duplicates() {
        List<Integer> numbers = new ArrayList<>();
        numbers.addAll(List.of(10, 20, 10, 30, 80, 70, 1, 70));

        List<Integer> expected = List.of(10, 20, 30, 80, 70, 1);
        List<Integer> actual = collectionImpl.distinct(numbers);

        assertEquals(expected.size(), actual.size());
    }

    @Test
    public void distinct_should_return_string_list_without_duplicates() {
        List<String> strings = new ArrayList<>();
        strings.addAll(List.of("aaa", "bbb", "ccc", "ddd", "aaa"));

        List<String> expected = List.of("aaa", "bbb", "ccc", "ddd");
        List<String> actual = collectionImpl.distinct(strings);

        assertEquals(expected.size(), actual.size());
        for (String str : actual) {
            assertTrue(expected.contains(str));
        }
    }

    @Test
    public void distinct_should_return_user_list_without_duplicates() {
        List<User> users = new ArrayList<>();
        users.add(createUser("Bob","boo", false,
                LocalDate.of(2009, 1, 12)));
        users.add(createUser("Bill","boo", true,
                LocalDate.of(1994, 11, 5)));
        users.add(createUser("Bob","boo", false,
                LocalDate.of(2009, 1, 12)));
        users.add(createUser("Ben","boo", true,
                LocalDate.of(1989, 10, 25)));

        List<User> expected = List.of(
                createUser("Bob","boo", false,
                        LocalDate.of(2009, 1, 12)),
                createUser("Bill","boo", true,
                        LocalDate.of(1994, 11, 5)),
                createUser("Ben","boo", true,
                        LocalDate.of(1989, 10, 25))
        );

        List<User> actual = collectionImpl.distinct(users);
        assertEquals(expected.size(), actual.size());
        for (User user : actual) {
            assertTrue(expected.contains(user));
        }

    }

    @Test
    public void reduce_should_return_sum_of_all_int_list() {
        List<Integer> elements = List.of(1, 2, 25, 36, -50, 10);
        int expected = 24;
        assertTrue((collectionImpl.reduce(elements, Integer::sum).isPresent()));
        int actual = (collectionImpl.reduce(elements, Integer::sum)).get();
        assertEquals(expected, actual);
}

    @Test
    public void reduce_should_return_the_longest_string() {
        List<String> strings = List.of("foo", "baz", "aaaa", "ooooo");
        String expected = "ooooo";

        assertTrue(collectionImpl.reduce(strings, (first, second) ->
                first.length() > second.length() ? first : second).isPresent());
        String actual = collectionImpl.reduce(strings, (first, second) ->
                first.length() > second.length() ? first : second).get();
        assertEquals(expected, actual);
    }

    @Test
    public void reduce_should_return_youngest_married_user() {
        List<User> users = List.of(
                createUser("Bob","boo", false,
                        LocalDate.of(2009, 1, 12)),
                createUser("Bill","boo", true,
                        LocalDate.of(1994, 11, 5)),
                createUser("Bob","boo", false,
                        LocalDate.of(2009, 1, 12)),
                createUser("Ben","boo", true,
                        LocalDate.of(1989, 10, 25)));

        User expected = createUser("Bill","boo", true,
                LocalDate.of(1994, 11, 5));

        assertTrue(collectionImpl.reduce(collectionImpl.filter(users, User::isMarried),
                (first, second) -> dateProvider.calculateWholeYearsCountFromPeriod(first.getBirthday()) < dateProvider.calculateWholeYearsCountFromPeriod(second.getBirthday())
                        ? first : second).isPresent());
        User actual = collectionImpl.reduce(collectionImpl.filter(users, User::isMarried),
                (first, second) -> dateProvider.calculateWholeYearsCountFromPeriod(first.getBirthday()) < dateProvider.calculateWholeYearsCountFromPeriod(second.getBirthday())
                        ? first : second).get();
        assertEquals(expected, actual);
    }

    @Test
    public void reduce_should_return_sum_of_int() {
        List<Integer> elements = List.of(1, 2, 25, 36, -50, 10);
        int expected = 24;
        int actual = (collectionImpl.reduce(0, elements, Integer::sum));
        assertEquals(expected, actual);
    }

    @Test
    public void reduce_should_return_string_built_from_other_strings() {
        List<String> strings = List.of(" returns", " one", " value", " from", " stream");
        String expected = "reduction returns one value from stream";
        String actual = collectionImpl.reduce("reduction", strings, String::concat);
        assertEquals(expected, actual);
    }

    @Test
    public void reduce_should_return_average_user_age() {
        List<User> users = List.of(
                createUser("Bob","boo", false,
                        LocalDate.of(2009, 1, 12)),
                createUser("Bill","boo", true,
                        LocalDate.of(1994, 11, 5)),
                createUser("Ben","boo", true,
                        LocalDate.of(1989, 10, 25)));
        int expected = 22;
        List<Integer> ages = users.stream().map(user ->dateProvider.calculateWholeYearsCountFromPeriod(user.getBirthday())).collect(Collectors.toList());
        int actual = collectionImpl.reduce(0, ages, (first, second) -> first + second)/users.size();
        assertEquals(expected, actual);
    }

    @Test
    public void partitionBy_should_return_map_with_two_int_lists() {
        List<Integer> elements = List.of(1, 2, 25, 36, -50, 10);
        Map<Boolean, List<Integer>> expected = Map.of(
                false, List.of(25, 36, 10),true, List.of(1, 2, -50));
        Map<Boolean, List<Integer>> actual = collectionImpl.partitionBy(elements, num -> num < 10);
        assertEquals(expected, actual);
    }

    @Test
    public void partitionBy_should_return_map_with_two_string_list() {
        List<String> strings = List.of("foo", "baz", "aaaa", "ooooo");
        Map<Boolean, List <String>> expected = Map.of(
                true, List.of("foo", "ooooo"), false, List.of("baz", "aaaa"));
        Map <Boolean, List<String>> actual = collectionImpl
                .partitionBy(strings, str -> str.contains("oo"));
        assertEquals(expected, actual);
    }

    @Test
    public void partitionBy_should_return_map_with_two_users_list() {
        List<User> users = List.of(
                createUser(LocalDate.of(2009, 1, 12)),
                createUser(LocalDate.of(1994, 11, 5)),
                createUser(LocalDate.of(1989, 10, 25))
        );
        Map<Boolean, List<User>> expected = Map.of(true, List.of(createUser(LocalDate.of(2009, 1, 12))),
                false, List.of(
                        createUser(LocalDate.of(1994, 11, 5)),
                        createUser(LocalDate.of(1989, 10, 25))
                ));
        Map<Boolean, List<User>> actual = collectionImpl.partitionBy(
                users,
                user -> user.getBirthday().getYear() > 2000
        );
        assertEquals(expected, actual);
    }

    @Test
    public void partitionBy_should_return_map_with_two_string_lists_one_of_them_should_be_empty() {
        List<String> strings = List.of("foo", "baz", "aaaa", "ooooo");
        Map<Boolean, List <String>> expected = Map.of(
                false, List.of("foo", "baz", "aaaa", "ooooo"), true, List.of());
        Map <Boolean, List<String>> actual = collectionImpl.partitionBy(
                strings,
                str -> str.length() > 5
        );
        assertTrue(actual.get(true).isEmpty());
        assertEquals(expected, actual);
    }

    @Test
    public void groupBy_should_return_int_map_grouped_by_numbers() {
        List<Integer> numbers = List.of(1, 2, 25, 36, -50, 10);
        Map<Boolean, List<Integer>> expected = Map.of(
                true, List.of(1, 2, 25, 36, 10), false, List.of(-50));
        Map<Boolean,List<Integer>> actual = collectionImpl.groupBy(
                numbers,
                number -> number > 0
        );
        assertEquals(expected, actual);
    }

    @Test
    public void groupBy_should_group_strings_by_length() {
        List<String> strings = List.of("foo", "baz", "aaaa", "ooooo");
        Map<Integer, List<String>> expected = Map.of(3, List.of("foo", "baz"),
                4, List.of("aaaa"), 5, List.of("ooooo"));
        Map<Integer, List<String>> actual = collectionImpl.groupBy(
                strings,
                String::length
        );
        assertEquals(expected, actual);
    }

    @Test
    public void groupBy_should_group_users_by_name() {
        User userOne = createUser("Bob","boo", false,
                LocalDate.of(2009, 1, 12));
        User userTwo = createUser("Bill","boo", true,
                LocalDate.of(1994, 11, 5));
        User userThree = createUser("Bob","boo", false,
                LocalDate.of(2009, 1, 12));
        User userFour = createUser("Ben","boo", true,
                LocalDate.of(1989, 10, 25));
        List<User> users = List.of(userOne, userTwo, userThree, userFour);
        Map<String, List<User>> expected = Map.of("Bob", List.of(userOne, userThree),
                "Bill", List.of(userTwo), "Ben", List.of(userFour));
        Map<String, List<User>> actual = collectionImpl.groupBy(users, User::getFirstName);
        assertEquals(expected, actual);
    }

    @Test
    public void toMap_should_return_map_from_int_list(){
        List<Integer> numbers = List.of(10, 20, 58, 45, 69, 20, 25, 25);
        Map<Integer, String> expected = Map.of(
                40,"20duplicate", 138,"69", 50, "25duplicate",
                20, "10", 116, "58", 90,"45");
        Map<Integer, String> actual = collectionImpl.toMap(
                numbers,
                num -> num*2,
                Object::toString,
                (first, second) -> first + "duplicate"
        );
        assertEquals(expected, actual);
    }

    @Test
    public void toMap_should_return_map_from_string_list() {
        List<String> strings = List.of("foo", "baz", "aaaa", "ooooo", "foo");
        Map<Integer, String> expected = Map.of(
                3, "FOO BAZ FOO", 4, "AAAA", 5, "OOOOO"
        );
        Map<Integer, String> actual = collectionImpl.toMap(
                strings,
                String::length,
                String::toUpperCase,
                (first, second) -> first + " " + second
        );
        assertEquals(expected, actual);
    }

    @Test
    public void toMap_should_return_map_from_user_list() {
        List<User> users = List.of(
                createUser("Bob","boo", false,
                        LocalDate.of(2009, 1, 12)),
                createUser("Bill","boo", true,
                        LocalDate.of(1994, 11, 5)),
                createUser("Bob","boo", false,
                        LocalDate.of(2009, 1, 12)),
                createUser("Ben","boo", true,
                        LocalDate.of(1989, 10, 25)));
        Map<String, String> expected = Map.of(
                "Bob", "11, 11", "Bill", "25", "Ben", "30"
        );
        Map<String, String> actual = collectionImpl.toMap(
                users,
                User::getFirstName,
                user -> String.valueOf(dateProvider.calculateWholeYearsCountFromPeriod(user.getBirthday())),
                (first, second) -> first + ", " +second
        );
        assertEquals(expected, actual);
    }

    @AfterEach
    public void tearDown() {
        collectionImpl = null;
        dateProvider = null;
    }

}

