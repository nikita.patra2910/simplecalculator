package simple;

import user.dto.User;

import java.time.LocalDate;

public class UserFactory {

    public static User createUser(
            String firstName,
            String lastName,
            boolean isMarried,
            LocalDate birthday
    ) {
        var user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMarried(isMarried);
        user.setBirthday(birthday);

        return user;
    }

    public static User createUser(LocalDate birthday) {
        return createUser("foo","boo", false, birthday);
    }

    public static User createUser(
            String firstName,
            boolean isMarried
    ) {
        return createUser(firstName, "foo", isMarried, LocalDate.now());
    }

    public static User createUser(String firstName, LocalDate birthday) {
        return createUser(firstName, "foo", false, birthday);
    }
}