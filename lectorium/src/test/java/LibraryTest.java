import operations.Lecture;
import operations.Library;
import operations.Resources;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class LibraryTest {

    private Library library;

    @BeforeEach
   public void setUp() {
        library = new Library();
    }

    @Test
    void getAllLectures() {
        Lecture lecture1 = new Lecture("Java Core", List.of(new Resources("Effective Java")));
        Lecture lecture2 = new Lecture("JDBC", List.of());
        library.addLecture(lecture1);
        library.addLecture(lecture2);
        List<String> expected = List.of("Java Core", "JDBC");
        List<String> actual = library.getAllLectures().stream().map(Lecture::getLectureName).collect(Collectors.toList());
        assertEquals(expected, actual);
    }

    @Test
    void addLecture() {
        assertTrue(library.getAllLectures().isEmpty());
        Lecture lecture1 = new Lecture("Java Core", List.of(new Resources("Effective Java")));
        library.addLecture(lecture1);
        assertEquals(1, library.getAllLectures().size());
    }

    @Test
    void removeLecture() {
        Lecture lecture1 = new Lecture("Java Core", List.of(new Resources("Effective Java")));
        Lecture lecture2 = new Lecture("JDBC", List.of());
        library.addLecture(lecture1);
        library.addLecture(lecture2);
        Lecture removingLecture = library.getLecture(1);
        library.removeLecture(1);
        library.getAllLectures().forEach(lecture -> assertNotEquals(lecture, removingLecture));
    }

    @Test
    void getLecture() {
        Lecture lecture1 = new Lecture("Java Core", List.of(new Resources("Effective Java")));
        Lecture lecture2 = new Lecture("JDBC", List.of());
        library.addLecture(lecture1);
        library.addLecture(lecture2);
        assertEquals(lecture1, library.getLecture(0));
    }
}