package operations;

public class Resources {
String sourceName;

    public Resources(String sourceName) {
        this.sourceName = sourceName;
    }

    @Override
    public String toString() {
        return "source: " + sourceName;
    }
}
