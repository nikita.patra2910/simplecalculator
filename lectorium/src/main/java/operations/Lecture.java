package operations;

import java.util.List;

public class Lecture {

    private List<Resources> sources;
    private String lectureName;

    public Lecture(String lectureName, List<Resources> sources) {
        this.lectureName = lectureName;
        this.sources = sources;
    }

    public String getLectureName() {
        return lectureName;
    }

    public void addSources(List<Resources> newSources) {
        sources.addAll(newSources);
    }

    public void removeSources(List<Resources> sourcesForRemove) {
        for (Resources resource: sourcesForRemove) {
            if (sources.stream()
                    .map(s -> s.sourceName).noneMatch(s -> s.equals(resource.sourceName))) {
                System.out.println("There are no such content in the lecture. Try again \n");
            }
            else {
                Resources sourceForRemove = sources.stream()
                        .filter(s -> s.sourceName.equals(resource.sourceName))
                        .findFirst().get();
                sources.remove(sourceForRemove);
                System.out.println(resource.toString() + " was removed");
            }
        }
    }

    public List<Resources> getSources() {
        for (Resources source: sources) {
            System.out.println(source.toString());
        }
        return sources;
    }

    @Override
    public String toString() {
        return "Lecture{" +
                "sources=" + sources +
                ", lectureName='" + lectureName + '\'' +
                '}';
    }
}
