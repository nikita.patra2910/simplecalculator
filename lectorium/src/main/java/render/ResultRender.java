package render;

import operations.Lecture;
import operations.Resources;

import java.util.List;
import java.util.Scanner;

import static java.util.Objects.isNull;

public class ResultRender {
    private final Scanner scanner;

    public ResultRender() {
        this.scanner = new Scanner(System.in);
    }

    public void render() {
        System.out.println("The operation was performed successfully");
       }
    
    public void render(List<Lecture> lectures){
        if (isNull(lectures) || lectures.isEmpty()) {
            System.out.println("There are no lectures in the lectorium");
        } else {
            for (int i = 0; i < lectures.size(); i++) {
                System.out.printf("%s" + " " + "%s%n", i + 1, lectures.get(i).getLectureName());
            }
        }
    }
    
    public void render(Lecture lecture) {
        System.out.printf("Selected lecture is %s%n", lecture.getLectureName());
    }

    public void menuRender() {
        System.out.println("Select Operation: \n" +
                "1 - Get all Lectures \n" +
                "2 - Add new Lecture \n" +
                "3 - Remove Lecture by its number \n" +
                "4 - Get Lecture by its number \n" +
                "5 - Get lecture resources \n" +
                "6 - Add lecture resources \n" +
                "7 - Remove lecture resources \n" +
                "8 - Go to Main Menu");
    }

    public void renderSources(List<Resources> sources){
        if (isNull(sources) || sources.isEmpty()) {
            System.out.println("There are no sources available");
        } else {
            for (int i = 0; i < sources.size(); i++) {
                System.out.printf("%s" + " " + "%s%n", i + 1, sources.get(i).toString());
            }
        }
    }

    public void renderSources () {
        System.out.println("");
    }
    
}
