package commands;

import operations.Lecture;
import operations.Resources;
import render.ResultRender;

import java.util.List;

public class AddLectureResourcesCommand implements Command {

    private final Lecture lecture;
    private final List <Resources> resources;
    private final ResultRender render;

    public AddLectureResourcesCommand(Lecture lecture, List <Resources> resources, ResultRender render) {
        this.lecture = lecture;
        this.resources = resources;
        this.render = render;
    }

    @Override
    public void execute() {
        lecture.addSources(resources);
        render.renderSources();
    }
}
