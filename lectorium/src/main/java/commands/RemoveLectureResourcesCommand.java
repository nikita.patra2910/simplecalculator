package commands;

import operations.Lecture;
import operations.Resources;
import render.ResultRender;

import java.util.List;

public class RemoveLectureResourcesCommand implements Command {
    private final Lecture lecture;
    private final ResultRender render;
    private final List <Resources> sources;

    public RemoveLectureResourcesCommand(Lecture lecture, ResultRender render, List <Resources> sources) {
        this.lecture = lecture;
        this.render = render;
        this.sources = sources;
    }

    @Override
    public void execute() {
        lecture.removeSources(sources);
        render.render();
    }
}
