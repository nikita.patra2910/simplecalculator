package commands;

import operations.Lecture;
import operations.Library;
import render.ResultRender;

public class AddLecturesCommand implements Command {

    private final Library library;
    private final ResultRender render;
    private final Lecture lecture;

    public AddLecturesCommand(Library library, ResultRender render, Lecture lecture) {
        this.library = library;
        this.render = render;
        this.lecture = lecture;
    }

    @Override
    public void execute() {
     library.addLecture(lecture);
     render.render();
    }
}
