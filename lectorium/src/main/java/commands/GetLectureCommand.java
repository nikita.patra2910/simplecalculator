package commands;

import operations.Library;
import render.ResultRender;

import java.util.Scanner;

public class GetLectureCommand implements Command {

    private final Library library;
    private final ResultRender render;
    private final Scanner scanner;

    public GetLectureCommand(Library library, Scanner scanner, ResultRender render) {
        this.library = library;
        this.scanner = scanner;
        this.render = render;
    }

    @Override
    public void execute() {
        render.render(library.getLecture(scanner.nextInt()));
    }
}
