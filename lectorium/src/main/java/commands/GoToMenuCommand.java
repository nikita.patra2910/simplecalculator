package commands;

import render.ResultRender;

public class GoToMenuCommand implements Command {
    private ResultRender render;

    public GoToMenuCommand(ResultRender render) {
        this.render = render;
    }

    @Override
    public void execute() {
        render.render();
    }

}
