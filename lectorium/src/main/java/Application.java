import operations.Lecture;
import operations.Library;
import operations.Resources;

import java.util.ArrayList;
import java.util.List;

public class Application {

    public static void main(String[] args) {

        Library library = new Library();
        Console console = new Console(library);

        List<Resources> resources1 = new ArrayList<>();
        resources1.add(new Resources("Effective Java"));
        List<Resources> resources2 = new ArrayList<>();
        resources2.add(new Resources("Software Development"));
        List<Resources> resources3 = new ArrayList<>();
        resources3.add(new Resources("Java: Beginners Guide"));
        List<Resources> resources4 = new ArrayList<>();
        resources4.add(new Resources("Java. Professional Library"));

        Lecture lecture1 = new Lecture("Java Core", resources1);
        Lecture lecture2 = new Lecture("JDBC", resources2);
        Lecture lecture3 = new Lecture("OOP", resources3);
        Lecture lecture4 = new Lecture("Applets", resources4);

        library.addLecture(lecture1);
        library.addLecture(lecture2);
        library.addLecture(lecture3);
        library.addLecture(lecture4);

        console.performFirstLevelOperations(library);
    }
}
